import { AppRegistry } from 'react-native';
import App from './App';
import MyList from './src/examples/List'
import RealmRos from "./src/examples/RealmRos";
import RealmES6Classes from './src/examples/RealmES6Classes'

AppRegistry.registerComponent('AwesomeProjectTS', () => RealmES6Classes);

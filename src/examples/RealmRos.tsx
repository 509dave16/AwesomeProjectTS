import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    FlatList, ListViewDataSource, ListView,
} from 'react-native';
import config from './config';
import Realm from 'realm';
import uuid from 'uuid/v4';
import { ListItem, List, Button as NBButton, Icon } from 'native-base';

interface Props { }
interface State { realm: Realm|null; dogs: any[]; status: string; }
export default class RealmRos extends Component<Props, State> {
    public listDataSource: ListViewDataSource;
    constructor(props) {
        super(props);
        this.listDataSource = new ListView.DataSource({ rowHasChanged: (item1, item2) => item1.name !== item2.name});
    }

    componentWillMount() {
        this.setState({ realm: null, dogs: [], status: 'Loading' });
        this.realmUserDefaultLogin(this.realmDefaultOpen, (e) => {
            const isTypeError = e instanceof TypeError;
            const isFailedNetworkRequest = e.message === 'Network request failed';
            if (isTypeError && isFailedNetworkRequest) {
                this.setState({ status: 'Server could not be reached.'} );
            } else if (isTypeError) {
                this.setState({ status: e.message });
            } else {
                this.setState({ status: 'Unknown error'});
            }
        });
    }

    realmDefaultOpen = (user) => {
        console.log('opening realm');
        Realm.open({
            path: 'my-realm',
            sync: {
                user,
                url: config.rosRealmUrl,
            },
            schema: [{name: 'Dog', properties: {name: 'string'}}]
        })
            .catch((error) => {
                console.log(error);
            })
            .then((realm: Realm) => {
                const dogs = realm.objects('Dog');
                const self = this;
                var myFunc = function (collection, changes) {
                    console.log('collection', collection);
                    console.log('collectionLength', collection.length);
                    console.log('changes', changes);
                    self.forceUpdate();
                };
                myFunc.bind(this);
                dogs.addListener(myFunc);
                this.setState({ status: 'Loaded', dogs, realm });
            });
    };

    realmUserDefaultLogin(successCallback, errorCallback = (error) => {}) {
        Realm.Sync.User.login(config.rosLoginUrl, 'realm-admin', '').then(user => {
            successCallback(user);
        }).catch(error => {
            errorCallback(error);
        });
    }

    renderItem = ({ item }) => {
      return (
          <Text>{item.name}</Text>
      )
    };

    keyExtractor = (item) => {
      return item.name;
    };

    render() {
        const info = this.state.dogs
            ? 'Number of dogs in this realm: ' + this.state.dogs.length
            : 'Loading...';

        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    {info}
                </Text>
                <Text>
                    Status: {this.state.status}
                </Text>
                {/*<FlatList data={this.state.dogs} renderItem={this.renderItem} keyExtractor={this.keyExtractor} extraData={uuid()}/>*/}
                { this.createList(this.state.realm, this.listDataSource.cloneWithRows(this.state.dogs)) }
                <Button onPress={this.addDog} title="Add Rex"></Button>
                <Button onPress={this.removeFirstDog} title="Remove First"></Button>
            </View>
        );
    }

    addDog = () => {
        try {
            this.state.realm.write(() => {
                this.state.realm.create('Dog', { name: `Rex-${uuid()}` });
            });
            // this.forceUpdate();
        } catch (e) {
            console.log("Error on creation of Dog");
            console.log(e);
        }
    };

    removeFirstDog = () => {
        try {
            this.state.realm.write(() => {
                this.state.realm.delete(this.state.dogs[0]);
            });
            // this.forceUpdate();
        } catch (e) {
            console.log("Error on removal of first Dog.");
            console.log(e);
        }
    };
    createList = (realm: Realm, dataSource: ListViewDataSource) => {
        const pressRight = (data, secId, rowId, rowMap) => {
            return () => {
                const key: string = `${secId}${rowId}`;
                rowMap[key].props.closeRow();
                realm.write(() => {
                    realm.delete(data);
                });
                // this.forceUpdate();
                // const newData = [...this.state.dogs];
                // newData.splice(rowId, 1);
                // this.setState({ dogs: newData });

            };
        };
        const renderRight = (data, secId, rowId, rowMap) => (
            <NBButton full danger onPress={pressRight(data, secId, rowId, rowMap)}>
                <Icon active name='trash' />
            </NBButton>
        );

        const pressLeft = (data, secId, rowId, rowMap) => {
            return () => {
                rowMap[`${secId}${rowId}`].props.closeRow();
            };
        };
        const renderLeft = (data, secId, rowId, rowMap) => (
            <NBButton full onPress={pressLeft(data, secId, rowId, rowMap)}>
                <Icon active name='create' />
            </NBButton>
        );

        const renderRow = (data) => (
            <ListItem style={{ alignSelf: 'center'}}>
                <Text> {data.name} </Text>
            </ListItem>
        );

        return (
            <List
                removeClippedSubviews={true}
                dataSource={dataSource}
                renderRow={renderRow}
                renderLeftHiddenRow={renderLeft}
                renderRightHiddenRow={renderRight}
                leftOpenValue={75}
                rightOpenValue={-75}
            />

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

import React, { Component } from 'react';
import {
    StyleSheet, Text, View, TextInput, Button, FlatList, TouchableOpacity, ListViewDataSource,
    ListView
} from 'react-native';
import { List, Icon, ListItem, Button as NBButton } from 'native-base';

interface Props { }
interface State { items: any[], name: string }
export default class MyList extends Component<Props, State> {
    public listDataSource: ListViewDataSource;
    constructor(props) {
        super(props);
        this.state = { items: [], name: ''};
        this.listDataSource = new ListView.DataSource({ rowHasChanged: (item1, item2) => item1.uuid !== item2.uuid});
    }

    componentWillMount() {}

    onNameChange = (text) => {
        this.setState({ name: text})
    };

    renderItem = ({ item }) => {
        console.log(item);
        return (
            <Text>{item.name}</Text>
        );
    };

    keyExtractor = (item) => {
      return item.id;
    };

    render() {
        return (
            <View style={{ flex: 1}}>
                <View style={{ flexDirection: 'row'}}>
                    <Text>Name</Text>
                    <TextInput value={this.state.name} onChangeText={this.onNameChange}/>
                </View>
                <Button title='Add Item' onPress={this.addItem}/>
                {/*<FlatList data={this.state.items} renderItem={this.renderItem} keyExtractor={this.keyExtractor}/>*/}
                { this.createList(this.listDataSource.cloneWithRows(this.state.items)) }
                <Button title='Remove First' onPress={this.removeFirstItem}/>
            </View>
        );
    }

    addItem = () => {
        const items = [...this.state.items];
        items.push({ id: items.length, name: this.state.name });
        console.log(items.length);
        this.setState({ items, name: '' });
    };

    removeFirstItem = () => {
        let items = [...this.state.items];
        items = items.slice(1);
        this.setState({ items });
    };

    createList = (dataSource: ListViewDataSource) => {
        const pressRight = (data, secId, rowId, rowMap) => {
            return () => {
                const key: string = `${secId}${rowId}`;
                rowMap[key].props.closeRow();
                const newData = [...this.state.items];
                newData.splice(rowId, 1);
                this.setState({ items: newData });

            };
        };
        const renderRight = (data, secId, rowId, rowMap) => (
            <NBButton full danger onPress={pressRight(data, secId, rowId, rowMap)}>
                <Icon active name='trash' />
            </NBButton>
        );

        const pressLeft = (data, secId, rowId, rowMap) => {
            return () => {
                rowMap[`${secId}${rowId}`].props.closeRow();
            };
        };
        const renderLeft = (data, secId, rowId, rowMap) => (
            <NBButton full onPress={pressLeft(data, secId, rowId, rowMap)}>
                <Icon active name='create' />
            </NBButton>
        );

        const renderRow = (data) => (
            <ListItem style={{ alignSelf: 'center'}}>
                <Text> {data.name} </Text>
            </ListItem>
        );

        return (
            <List
                removeClippedSubviews={true}
                dataSource={dataSource}
                renderRow={renderRow}
                renderLeftHiddenRow={renderLeft}
                renderRightHiddenRow={renderRight}
                leftOpenValue={75}
                rightOpenValue={-75}
            />

        );
    }
}


const styles = StyleSheet.create({});

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

class DogModel {
    public static schema: Realm.ObjectSchema = {name: 'Dog', properties: {name: 'string'} };
    public name: string ;
    getName() {
        return this.name;
    }
    setName(name) {
        this.name = name;
    }
}

const Realm = require('realm');

interface Props { }
interface State { realm: Realm, keysOfElementPassedToOpen: string[], keysOfElementNotPassedToOpen: string[] }
export default class RealmES6Classes extends Component<Props, State> {
    public schema: Realm.ObjectClass[] = [DogModel];
    public models: Realm.ObjectClass[] = [DogModel];
    constructor(props) {
        super(props);
        this.state = {
            realm: null,
            keysOfElementPassedToOpen: [],
            keysOfElementNotPassedToOpen: this.getClassKeys('models'),
        };
    }

    componentWillMount() {
        this.realmDefaultOpen();
    }

    getClassKeys(prop): string[] {
        const obj = this[prop][0];
        let propKeys = Object.keys(obj);
        let funcKeys = obj.prototype ? Object.getOwnPropertyNames(obj.prototype) : [];
        return [...propKeys, ...funcKeys];
    }

    realmDefaultOpen = () => {
        Realm.open({
            path: 'realm-es6-classes',
            schema: this.schema
        })
        .catch((error) => {
            console.log(error);
        })
        .then(realm => {
            this.setState({ realm, keysOfElementPassedToOpen: this.getClassKeys('schema') });
        });
    };

    render() {
        const info = this.state.realm ? 'Realm open' : 'Realm opening...';

        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    {info}
                </Text>
                <View>
                    <Text style={styles.welcome}>
                        Keys of element not passed to open: {this.state.keysOfElementNotPassedToOpen.join(',')}
                    </Text>
                    <Text style={styles.welcome}>
                        Keys of element passed to open: {this.state.keysOfElementPassedToOpen.join(',')}
                    </Text>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});